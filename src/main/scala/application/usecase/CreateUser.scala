package application.usecase

import application.exception.UserAlreadyExistsException
import application.port.UserRepository
import domain.User

class CreateUser(val userRepository: UserRepository) {
  val initialState=0;
  def create(user:User):Option[User]= {

    if (userRepository.userExists(user.name) )throw new UserAlreadyExistsException(user.name)

    val newUser=User(user.name,initialState)
    userRepository.create(newUser);
  }


}
