package application.usecase

import application.port.UserRepository
import domain.User

class UserExitsCase(val userRepository: UserRepository)  {

  def userExists(id:String):Boolean={
    userRepository.userExists(id)

  }

}
