package application.usecase

import application.port.BoardGameRepository
import domain.DiceRoll

class ThrowDiceUseCase (val boardGameRepository: BoardGameRepository) {

  def throwDice(dice:DiceRoll):Option[DiceRoll]={
    val randomRoll= scala.util.Random

    val one= 1 + randomRoll.nextInt(( 6-1) + 1);
    val two= 1 + randomRoll.nextInt(( 6-1) + 1);

    val newDiceRoll=DiceRoll(one,two)
    boardGameRepository.throwDice(newDiceRoll)

  }

}
