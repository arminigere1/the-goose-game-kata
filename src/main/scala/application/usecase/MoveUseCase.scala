package application.usecase

import application.port.BoardGameRepository
import domain.User


class MoveUseCase (val boardGameRepository: BoardGameRepository) {


  def move(moveTo:Int,player:User):Int={
    boardGameRepository.move(moveTo,player);
    }

}
