package application.port

import domain.User

trait UserRepository {

  def create (user:User):Option[User]

  def userExists(id:String):Boolean
}
