package application.port

import domain.{DiceRoll, User}

trait BoardGameRepository {

  def move(moveTo:Int,player:User):Int

  def throwDice(dice:DiceRoll):Option[DiceRoll]

}
