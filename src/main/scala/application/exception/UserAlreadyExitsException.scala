package application.exception

class UserAlreadyExistsException(val name: String) extends RuntimeException(name) {
}
