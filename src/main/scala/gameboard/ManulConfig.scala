package gameboard


import application.usecase.{CreateUser, UserExitsCase, ThrowDiceUseCase}
import dataaccess.{GooseGameManager, InMemoryPlayerRepository}


class ManulConfig{
  private val userRepository = new InMemoryPlayerRepository()
  private val gooseMap = new GooseGameManager()

def createUser() : CreateUser = {
  new CreateUser (userRepository)
}

  def findUser() : UserExitsCase = {
    new UserExitsCase (userRepository)
  }

 def  throwDice():ThrowDiceUseCase={
   new ThrowDiceUseCase(gooseMap)
 }

}
