  package gameboard

  import domain.{DiceRoll, User}

  object GameBoard extends App {


    // Variabile used
     val  config= new ManulConfig()
    val rollDice= config.throwDice()
    val createNewUser = config.createUser
    val findUser = config.findUser()
    var  winner:Boolean=false
    var playerOne=User("",0);




    /**
     *  create User  function
     */
    createPlayer()

    System.out.println("Player "+ playerOne.name + "  was added to game");

    do
    {

      System.out.println();

      System.out.println("(1) Start Goose Game");
      System.out.println("(2) Exit Game");
      System.out.println("Please select  a  Choice : ");
    val  choice = scala.io.StdIn.readLine();

      choice match
      {
        case "1" => {
          System.out.println(" Start game");

          do{
            /**
             *  1. player rolls dice
             *  2. player moves to position
             *  3.  do the controlls for certain directions
             *
             *  if uses lands on spaces  5, 9 , 14, 18  player moves again by the dices rolled
             *  before
             */

            System.out.println(playerOne.name + " on space " + playerOne.initialState);
            System.out.println();

            System.out.println(playerOne.name + " Rolls Dice");

            val diceRoll= rollDice.throwDice(DiceRoll(0,0))
            val rollValue=  diceRoll.get
            val moveNspaces= rollValue.rollOne + rollValue.rollTwo

            val moveplayer = rollDice.boardGameRepository.move(moveNspaces,playerOne)


            moveplayer match {

              case 63 => {
                winner=true
                System.out.println(playerOne.name +" rolls " + rollValue.rollOne +", "+ rollValue.rollTwo +".  "+  playerOne.name + " moves from "+playerOne.initialState + " to "+moveplayer);
                System.out.println()
                System.out.println("****** " +
                  playerOne.name.toUpperCase() + " IS THE WINNER "+"**********")
                 }
              case 6 => {
                System.out.println(playerOne.name + " rolls " + rollValue.rollOne + ", " + rollValue.rollTwo + ".  " + playerOne.name + " moves from " + playerOne.initialState + " to Bridge." +"  Jumps to " + moveplayer);

                playerOne.initialState = moveplayer

              }
              case _ =>{
                if (moveplayer != 0) {
                  System.out.println(playerOne.name + " rolls " + rollValue.rollOne + ", " + rollValue.rollTwo + ".  " + playerOne.name + " moves from " + playerOne.initialState + " to " + moveplayer);

                  playerOne.initialState = moveplayer

                } else {

                  System.out.println(playerOne.name + " rolls " + rollValue.rollOne + ", " + rollValue.rollTwo + ".  " + playerOne.name + " Does not moves from " + playerOne.initialState + " Because did not get 63");

                }

              }
           }


         }while(!winner)






       }
       case "2" => {
         System.out.println("Exiting game play..... ");
         System.out.println("James  has exited  game  ");
         System.exit(0);

       }



       case _ =>{ System.out.print("Wrong Choice!!!\n\n")
         System.out.print("Please select another choice from the menu\n\n")
       }
     }
   }while(scala.io.StdIn.readInt() != 2);


   /**
    * CREATE USER FUNCTION
    */
   def createPlayer() = {

     System.out.println("** WELCOME TO GOOSE GAME BETA VERSION**");
     System.out.println();
     System.out.println("** PLEASE ENTER NAME TO BEGIN PLAY**");
     System.out.println("Enter name : ");

     val  playerName = scala.io.StdIn.readLine();
     val user = User(playerName,0)
     // Create a user
     val   actualCreateUser = createNewUser.create(user).get
     playerOne=actualCreateUser
   }



  }


