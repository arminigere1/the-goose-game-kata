package dataaccess

import java.util

import application.port.UserRepository
import domain.User

class InMemoryPlayerRepository  extends  UserRepository{
  private val inMemoryDb = new util.HashMap[String, User]

  def create(user: User): Option[User] = {
    inMemoryDb.put(user.name,user)
    Some(user)
  }

   def userExists(id: String): Boolean =  inMemoryDb.containsKey(id)
}
