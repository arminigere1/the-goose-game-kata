import application.usecase.CreateUser
import dataaccess.InMemoryPlayerRepository
import domain.User

object CreateUserTest  extends App {

  private val userRepository = new InMemoryPlayerRepository

  def createUser = new CreateUser(userRepository)



  // Setup

  val createNewUser = CreateUserTest.createUser

  val user = User("Pippo",0)

  // Create a user
  val actualCreateUser = createNewUser.create(user)

  println(actualCreateUser.get)

}