# Goose Game 

The goose game is a game where two or more players move pieces around a track by rolling a die.
 The aim of the game is to reach square number sixty-three before any of the other players and avoid obstacles. (wikipedia)
 
 ## Technologies
 The game is written in Scala and uses SBT Build.
 
 ## Instruction for  Excecuting the Game
 To run the game, inport the project into your local workspace .
  go into the package src/main/scala/gameboard  and  execute the object class
 GameBoard.
 
 follow the instruction from the terminal. at the moment of creation the game
 only handles one player. 
 ## Future implementation
 in future implementation  the game  will be able to handle more  than one player
at the moment the game only handles one play.